var passport = require('passport');

/**
 * AuthController
 *
 * @description :: Server-side logic for managing auths
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	twitter: passport.authenticate('twitter'),
  error: error
};

function error(req, res) {
  return res.json({message: 'Oh NOES, you are not authenticated or cancelled!'});
}