var passport = require('passport');

module.exports = function(req, res, next) {
  return passport.authenticate('twitter', {failureRedirect: '/auth/error'}, function(err, userProfile, token) {
    // Grab the done(null, token) from the strategy. Maybe validate against the DB here?
    console.log('Hey, listen! I got token ' + token + ' and user profile ' + JSON.stringify(userProfile));
    next();
  })(req, res, next);
};