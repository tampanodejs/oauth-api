var passport = require('passport');
var TwitterStrategy = require('passport-twitter').Strategy;

var twitter = new TwitterStrategy({
    consumerKey: process.env.TWITTER_APP_ID,
    consumerSecret: process.env.TWITTER_APP_SECRET,
    callbackURL: 'http://127.0.0.1:1337/twitter/callback'
  },
  function(token, tokenSecret, userProfile, done) {
    done(null, userProfile, token);
  });

module.exports = {
  twitter: twitter
};

